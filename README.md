# Nordic nRF5 SDK and Driver Library #

* Version 15.3.0 released in February 2019
* This peripheral driver library is for the Nordic nRF5 Device Series.
  It conforms to CMSIS Cortex-M4 Core Peripheral Access Layer
* Developed by Nordic Semiconductor ASA
* Library written in C and Assembler
* Library includes startup files that can be used with IAR, GCC, Keil, and SES
* Library includes open source libraries adapted to nRF5, such as FreeRTOS

### Who do I talk to? ###

* Steve Karg, Jason Joyce, Jeremy Belge use this library on DLM LMBC-650 and
  LMRC-611 and IPPAN3.